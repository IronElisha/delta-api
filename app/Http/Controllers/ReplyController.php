<?php

namespace App\Http\Controllers;

use App\User;
use App\Topic;
use App\Reply;
use App\Watcher;
use Illuminate\Http\Request;

class ReplyController extends Controller
{
    /**
     * Creates a new reply to a topic.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request, $id)
    {
        $this->validate($request, [
            'post_body' => 'required',
            'user_id' => 'required|numeric'
        ]);

        $user = User::find($request->get('user_id'));
        if (!$user) {
            return response()->json(['error' => 'User specified does not exist! Please use a valid user.'], 404);
        }

        $topic = Topic::find($id);
        if (!$topic) {
            return response()->json(['error' => 'Topic you are posting a reply to does not exist!'], 404);
        }

        $reply = Reply::create($request->all() + ['topic_id' => $topic->id]);
        if (!$reply) {
            return response()->json(['error' => 'Failed to create reply!'], 500);
        }

        // Adds the user posting the reply as a watcher to our topic.
        $WatcherController = new WatcherController();
        $WatcherController->add($topic->id, $user->id);

        // Notifies all existing watchers that a reply has been posted to the ticket.
        $watchers = Watcher::query()->where('topic_id', $topic->id)->get();
        foreach ($watchers as $watcher) {
            error_log("Sending email to User {$watcher->user_id}.");
        }

        return response()->json(['success' => 'Reply created successfully!', 'data' => $reply], 200);
    }

    /**
     * Updates an existing reply.
     *
     * @param Request $request
     * @param int $id
     * @param int $reply_id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id, $reply_id)
    {
        $this->validate($request, [
            'post_body' => 'required'
        ]);

        $topic = Topic::find($id);
        if (!$topic) {
            return response()->json(['error' => 'This topic no longer exists, so we can not reply to it.'], 404);
        }

        $reply = Reply::find($reply_id);
        if (!$reply) {
            return response()->json(['error' => 'Im sorry, but if the reply you are looking for is not found in the Jedi Archives, then it simply does not exist!'], 404);
        }

        // Only pass post_body to prevent updating unwanted data.
        $result = $reply->update(['post_body' => $request->get('post_body')]);
        if (!$result) {
            return response()->json(['error' => 'Failed to update reply!'], 500);
        }

        return response()->json(['success' => 'Reply updated successfully!', 'data' => $reply], 200);
    }

    /**
     * Lists all replies for a specific topic.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function list($id)
    {
        return response()->json(['success' => true, 'data' => Reply::query()->where('topic_id', $id)->get()], 200);
    }

    /**
     * Gets a specific reply.
     *
     * @param $reply_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($reply_id)
    {
        $reply = Reply::find($reply_id);
        if (!$reply) {
            return response()->json(['error' => 'Reply not found!'], 404);
        }

        return response()->json(['success' => true, 'data' => $reply], 200);
    }

    /**
     * Deletes a specific reply.
     *
     * @param $reply_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($reply_id)
    {
        $reply = Reply::find($reply_id);
        if (!$reply) {
            return response()->json(['error' => 'Reply not found!'], 404);
        }

        $result = $reply->delete();
        if (!$result) {
            return response()->json(['error' => 'Failed to delete reply!'], 500);
        }

        return response()->json(['success' => 'Reply deleted successfully!'], 200);
    }
}
