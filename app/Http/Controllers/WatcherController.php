<?php

namespace App\Http\Controllers;

use App\Watcher;
use App\User;
class WatcherController extends Controller
{
    /**
     * Creates a new watcher.
     *
     * @param $id
     * @param $user_id
     * @return \Illuminate\Http\JsonResponse
     */
    public function add($id, $user_id)
    {
        $watcher = Watcher::query()->where('topic_id', $id)->where('user_id', $user_id)->first();
        if ($watcher) {
            return response()->json(['error' => 'Watcher already exists for this topic!'], 405);
        }

        $user = User::find($user_id);
        if (!$user) {
            return response()->json(['error' => 'User does not exist to add as a watcher.'], 404);
        }

        $watcher = Watcher::create(['topic_id' => $id, 'user_id' => $user_id]);
        if (!$watcher) {
            return response()->json(['error' => 'Failed to create watcher!'], 500);
        }

        return response()->json(['success' => 'Watcher added successfully!', 'data' => $watcher], 200);
    }

    public function remove($id, $user_id)
    {
        $watcher = Watcher::query()->where('topic_id', $id)->where('user_id', $user_id)->first();
        if (!$watcher) {
            return response()->json(['error' => 'Watcher has already been removed!'], 410);
        }

        $result = $watcher->delete();
        if (!$result) {
            return response()->json(['error' => 'Failed to delete watcher!'], 500);
        }

        return response()->json(['success' => 'Watcher removed successfully!'], 200);
    }
}
