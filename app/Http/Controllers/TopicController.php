<?php

namespace App\Http\Controllers;

use App\User;
use App\Topic;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    /**
     * Creates a new topic.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'post_body' => 'required',
            'user_id' => 'required|numeric'
        ]);

        $user = User::find($request->get('user_id'));
        if (!$user) {
            return response()->json(['error' => 'User specified does not exist! Please use a valid user.'], 404);
        }

        $topic = Topic::create($request->all());
        if (!$topic) {
            return response()->json(['error' => 'Failed to create topic!'], 500);
        }

        // Adds a watcher to our topic.
        $WatcherController = new WatcherController();
        $WatcherController->add($topic->id, $request->get('user_id'));

        return response()->json(['success' => 'Topic added successfully!', 'data' => $topic], 200);
    }

    /**
     * Updates an existing topic.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $topic = Topic::find($id);
        if (!$topic) {
            return response()->json(['error' => 'Topic not found to update!'], 404);
        }

        $result = $topic->update($request->all());
        if (!$result) {
            return response()->json(['error' => 'Failed to update topic!']);
        }

        return response()->json(['success' => 'Topic updated successfully!', 'data' => $topic], 200);
    }

    /**
     * Lists all topics.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return response()->json(['success' => true, 'data' => Topic::all()], 200);
    }

    /**
     * Gets a specific topic.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function get($id)
    {
        $topic = Topic::find($id);
        if (!$topic) {
            return response()->json(['error' => 'Topic not found!'], 404);
        }

        return response()->json(['success' => true, 'data' => $topic], 200);
    }

    /**
     * Deletes a specific topic. Related data is removed via cascade delete.
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $topic = Topic::find($id);
        if (!$topic) {
            return response()->json(['error' => 'Topic not found!'], 404);
        }

        $result = $topic->delete();
        if (!$result) {
            return response()->json(['error' => 'Failed to delete topic!'], 500);
        }

        return response()->json(['success' => 'Topic and all associated data deleted successfully!'], 200);
    }
}
