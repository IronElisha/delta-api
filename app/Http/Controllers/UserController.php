<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Creates a new user.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request)
    {
        $this->validate($request, [
            'username' => 'required',
            'email' => 'required|email'
        ]);

        $user = User::create($request->all());
        if (!$user) {
            return response()->json(['error' => 'Failed to create user!'], 500);
        }

        return response()->json(['success' => true, 'data' => $user], 200);
    }

    /**
     * Updates an existing user.
     *
     * @param Request $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if (!$user) {
            return response()->json(['error' => 'User not found to update!'], 404);
        }

        $result = $user->update($request->all());
        if (!$result) {
            return response()->json(['error' => 'Failed to update user!']);
        }

        return response()->json(['success' => true, 'data' => $user], 200);
    }

    /**
     * Lists all users.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        return response()->json(['success' => true, 'data' => User::all()], 200);
    }
}
