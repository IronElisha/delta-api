<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class WatcherTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test adding watchers.
     *
     * @return void
     */
    public function testAddWatcher()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;
        $topic = json_decode($this->json('POST', 'topics', ['title' => 'test topic', 'post_body' => 'just a test!', 'user_id' => $user->id])->response->getContent())->data;

        $this->json('DELETE', 'topics/' . $topic->id . '/watchers/' . $user->id);

        $test = $this->json('POST', 'topics/' . $topic->id . '/watchers/' . $user->id);
        $test->seeStatusCode(200);
    }

    /**
     * Test removing watchers.
     *
     * @return void
     */
    public function testRemoveWatcher()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;
        $topic = json_decode($this->json('POST', 'topics', ['title' => 'test topic', 'post_body' => 'just a test!', 'user_id' => $user->id])->response->getContent())->data;

        $this->json('POST', 'topics/' . $topic->id . '/watchers/' . $user->id);

        $test = $this->json('DELETE', 'topics/' . $topic->id . '/watchers/' . $user->id);
        $test->seeStatusCode(200);
    }
}
