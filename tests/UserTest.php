<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class UserTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test creating a new user.
     *
     * @return void
     */
    public function testCreateUser()
    {
        $test = $this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com']);
        $test->seeStatusCode(200);

        $user = json_decode($test->response->getContent())->data;
        $this->assertEquals($user->username, 'testuser');
        $this->assertEquals($user->email, 'test@email.com');
    }

    /**
     * Test updating a user.
     *
     * @return void
     */
    public function testUpdateUser()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;

        $test = $this->json('PUT', 'users/' . $user->id, ['username' => 'newtestusername', 'email' => 'test2@email.com']);
        $test->seeStatusCode(200);

        $user = json_decode($test->response->getContent())->data;
        $this->assertEquals($user->username, 'newtestusername');
        $this->assertEquals($user->email, 'test2@email.com');
    }

    /**
     * Test retrieving a user list.
     *
     * @return void
     */
    public function testListUsers()
    {
        $this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com']);

        $test = $this->json('GET', 'users');
        $test->seeStatusCode(200);
        $this->assertGreaterThan(0, count(json_decode($test->response->getContent())->data));
    }
}
