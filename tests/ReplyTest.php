<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class ReplyTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test creating a new reply.
     *
     * @return void
     */
    public function testCreateReply()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;
        $topic = json_decode($this->json('POST', 'topics', ['title' => 'test topic', 'post_body' => 'just a test!', 'user_id' => $user->id])->response->getContent())->data;

        $test = $this->json('POST', 'topics/' . $topic->id . '/replies', ['post_body' => 'reply body!', 'user_id' => $user->id]);
        $test->seeStatusCode(200);

        $reply = json_decode($test->response->getContent())->data;
        $this->assertEquals('reply body!', $reply->post_body);
        $this->assertEquals($user->id, $reply->user_id);

        // Watcher should be automatically added.
        $test = $this->json('POST', 'topics/' . $topic->id . '/watchers/' . $user->id);
        $test->seeStatusCode(405);
    }

    /**
     * Test updating an existing reply.
     *
     * @return void
     */
    public function testUpdateReply()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;
        $topic = json_decode($this->json('POST', 'topics', ['title' => 'test topic', 'post_body' => 'just a test!', 'user_id' => $user->id])->response->getContent())->data;

        $reply = json_decode($this->json('POST', 'topics/' . $topic->id . '/replies', ['post_body' => 'reply body!', 'user_id' => $user->id])->response->getContent())->data;

        $test = $this->json('PUT', 'topics/' . $topic->id . '/replies/' . $reply->id, ['post_body' => 'new reply bod!']);
        $test->seeStatusCode(200);

        $reply = json_decode($test->response->getContent())->data;
        $this->assertEquals('new reply bod!', $reply->post_body);
    }

    /**
     * Test listing replies.
     *
     * @return void
     */
    public function testListReplies()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;
        $topic = json_decode($this->json('POST', 'topics', ['title' => 'test topic', 'post_body' => 'just a test!', 'user_id' => $user->id])->response->getContent())->data;

        $this->json('POST', 'topics/' . $topic->id . '/replies', ['post_body' => 'reply body!', 'user_id' => $user->id]);

        $test = $this->json('GET', 'topics/' . $topic->id . '/replies');
        $test->seeStatusCode(200);
        $this->assertGreaterThan(0, count(json_decode($test->response->getContent())->data));
    }

    /**
     * Test getting a specific reply.
     *
     * @return void
     */
    public function testGetReply()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;
        $topic = json_decode($this->json('POST', 'topics', ['title' => 'test topic', 'post_body' => 'just a test!', 'user_id' => $user->id])->response->getContent())->data;
        $reply = json_decode($this->json('POST', 'topics/' . $topic->id . '/replies', ['post_body' => 'reply body!', 'user_id' => $user->id])->response->getContent())->data;

        $test = $this->json('GET', 'topics/' . $topic->id . '/replies/' . $reply->id);
        $test->seeStatusCode(200);
    }

    /**
     * Test deleting a reply.
     *
     * @return void
     */
    public function testDeleteReply()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;
        $topic = json_decode($this->json('POST', 'topics', ['title' => 'test topic', 'post_body' => 'just a test!', 'user_id' => $user->id])->response->getContent())->data;
        $reply = json_decode($this->json('POST', 'topics/' . $topic->id . '/replies', ['post_body' => 'reply body!', 'user_id' => $user->id])->response->getContent())->data;

        $test = $this->json('DELETE', 'topics/' . $topic->id . '/replies/' . $reply->id);
        $test->seeStatusCode(200);
    }
}
