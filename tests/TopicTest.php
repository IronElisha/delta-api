<?php

use Laravel\Lumen\Testing\DatabaseTransactions;

class TopicTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * Test creating a new topic.
     *
     * @return void
     */
    public function testCreateTopic()
    {
        $user = $response = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;

        $test = $this->json('POST', 'topics', ['title' => 'testtopic', 'post_body' => 'topic body!', 'user_id' => $user->id]);
        $test->seeStatusCode(200);

        $topic = json_decode($test->response->getContent())->data;
        $this->assertEquals($topic->title, 'testtopic');
        $this->assertEquals($topic->post_body, 'topic body!');
        $this->assertEquals($topic->user_id, $user->id);
    }

    /**
     * Test updating a topic.
     *
     * @return void
     */
    public function testUpdateTopic()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;

        $topic = json_decode($this->json('POST', 'topics', ['title' => 'testtopic', 'post_body' => 'topic body!', 'user_id' => $user->id])->response->getContent())->data;

        $test = $this->json('PUT', 'topics/' . $topic->id, ['title' => 'newtest', 'post_body' => 'its fresh!']);
        $test->seeStatusCode(200);

        $newTopic = json_decode($test->response->getContent())->data;
        $this->assertEquals($newTopic->title, 'newtest');
        $this->assertEquals($newTopic->post_body, 'its fresh!');
    }

    /**
     * Test listing topics.
     *
     * @return void
     */
    public function testListTopic()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;

        $this->json('POST', 'topics', ['title' => 'testtopic', 'post_body' => 'topic body!', 'user_id' => $user->id]);

        $test = $this->json('GET', 'topics');
        $test->seeStatusCode(200);
        $this->assertGreaterThan(0, count(json_decode($test->response->getContent())->data));
    }

    /**
     * Test getting a specific topic.
     *
     * @return void
     */
    public function testGetTopic()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;

        $topic = json_decode($this->json('POST', 'topics', ['title' => 'testtopic', 'post_body' => 'topic body!', 'user_id' => $user->id])->response->getContent())->data;

        $test = $this->json('GET', 'topics/' . $topic->id);
        $test->seeStatusCode(200);

        $getTopic = json_decode($test->response->getContent())->data;
        $this->assertEquals($getTopic->title, 'testtopic');
        $this->assertEquals($getTopic->post_body, 'topic body!');
        $this->assertEquals($getTopic->user_id, $user->id);
    }

    /**
     * Test deleting a specific topic.
     *
     * @return void
     */
    public function testDeleteTopic()
    {
        $user = json_decode($this->json('POST', 'users', ['username' => 'testuser', 'email' => 'test@email.com'])->response->getContent())->data;

        $topic = json_decode($this->json('POST', 'topics', ['title' => 'testtopic', 'post_body' => 'topic body!', 'user_id' => $user->id])->response->getContent())->data;

        $test = $this->json('DELETE', 'topics/' . $topic->id);
        $test->seeStatusCode(200);
    }
}
