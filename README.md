### Delta API ###
Delta API is a REST API built on the Lumen Framework that powers a simple forum.
* Version 1.0

[Live API Endpoint:](http://delta-api.blueironlabs.com)

[API Documentation:](https://documenter.getpostman.com/view/12459633/T1LV9PUY?version=latest)

Dependencies

* PHP >= 7.2
* OpenSSL PHP Extension
* PDO PHP Extension
* Mbstring PHP Extension

### Getting Started ###
Delta API contains the Lumen framework and everything you need to get started. There are a couple steps needed to get up and running with the API, so follow the instructions and we'll come out on top!

* Set up a web server running your preference tech stack (with the requirement that you have PHP >= 7.2 as part of that stack).
* Configure your database installation (one of MySQL, PostgreSQL, SQLite, and SQL Server).
* Create a new database table for this project (called delta, for example).
* Configure Delta API with your environment specifics (.env file). A sample .env file can be found with this project.
* Copy all project files to your web server project directory (/var/www/html for example).
* Test Deployment! Once everything is configured and set up, you should be serving out the Delta API!

### Running Tests ###
There are 16 tests built into Delta API and they can be run at anytime.

* Navigate to the project root (/var/www/html for example).
* Run the following command: ./vendor/bin/phpunit

This should run any tests we have against the API.

## Documentation ##

### Questions ###
This is a proof of concept project utilizing the Lumen REST API framework to serve out endpoints for managing a simple forum. If there are any questions or concerns about this project, please email elishab@blueironlabs.com.