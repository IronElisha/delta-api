<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/*
|--------------------------------------------------------------------------
| User Endpoints
|--------------------------------------------------------------------------
*/
$router->group(['prefix' => 'users'], function() use ($router) {
    $router->get('', ['uses' => 'UserController@list']);
    $router->post('', ['uses' => 'UserController@create']);
    $router->put('{id}', ['uses' => 'UserController@update']);
});

/*
|--------------------------------------------------------------------------
| Topic / Reply Endpoints
|--------------------------------------------------------------------------
*/
$router->group(['prefix' => 'topics'], function() use ($router) {
    $router->get('', ['uses' => 'TopicController@list']);
    $router->post('', ['uses' => 'TopicController@create']);
    $router->get('{id}', ['uses' => 'TopicController@get']);
    $router->put('{id}', ['uses' => 'TopicController@update']);
    $router->delete('{id}', ['uses' => 'TopicController@delete']);
    $router->get('{id}/replies', ['uses' => 'TopicController@getReplies']);

    $router->get('{id}/replies', ['uses' => 'ReplyController@list']);
    $router->get('{id}/replies/{reply_id}', ['uses' => 'ReplyController@get']);
    $router->post('{id}/replies', ['uses' => 'ReplyController@create']);
    $router->put('{id}/replies/{reply_id}', ['uses' => 'ReplyController@update']);
    $router->delete('{id}/replies/{reply_id}', ['uses' => 'ReplyController@delete']);

    $router->post('{id}/watchers/{user_id}', ['uses' => 'WatcherController@add']);
    $router->delete('{id}/watchers/{user_id}', ['uses' => 'WatcherController@remove']);
});